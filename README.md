**Note:**

How to use the application:
1. Open LocalizationSampleWebAPI.sln
2. Click IIS Express to run the application into the browser.
3. Route for Home Controller with English Culture is **en-US/home**
4. Route for Home Controller with Germany Culture is **de-DE/home**
5. Route for About Controller with English Culture is **en-US/about**
6. Route for About Controller with Germany Culture is **de-DE/about**
